const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const Favorite = require('../models/favorite');
const authenticate = require('../authenticate');
const cors = require('./cors');
const Dishes = require('../models/dishes');


const favRouter = express.Router();
favRouter.use(bodyParser.json());

favRouter.route('/')
.options(cors.corsWithOptions,(req,res)=>{
    res.sendStatus(200);})
.get(cors.corsWithOptions,authenticate.verifyUser,(req,res,next)=>{
    Favorite.findOne({user : req.user._id})
    .then((favs)=>{
        if(favs!==null)
        {
            Favorite.findById(favs._id)
            .populate('user')
            .populate('dishes')
            .then((favorite) => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json(favorite);
            })
        }else{
            err = new Error('You have no favorites!');
            err.status = 404;
            return next(err);
        }
    })
})

.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /dishes');
})

.post(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {  
         Favorite.findOne({user:req.user._id})
         .then((favs)=>{
             if(!favs){
             Favorite.create({user:req.user._id})
             .then((favs)=>{
                for (i = 0; i < req.body.length; i++) {
                    if (favs.dishes.indexOf(req.body[i]._id) < 0) {
                        favs.dishes.push(req.body[i]);
                    }
                }
                favs.save()
                .then((favs) => {
                    Favorite.findById(favs._id)
                    .populate('user')
                    .populate('dishes')
                .then((favs)=>{
                res.statusCode = 200;
                res.setHeader('Content-Type','application/json')
                res.json(favs); 
                },(err)=>next(err))
            },(err)=>next(err))
          },(err)=>next(err))
             }else{
                for (i = 0; i < req.body.length; i++) {
                    if (favs.dishes.indexOf(req.body[i]._id) < 0) {
                        favs.dishes.push(req.body[i]);
                    }
                }
                favs.save()
                .then((favs)=>{
                    Favorite.findById(favs._id)
                    .populate('user')
                    .populate('dishes')
                    .then((favs)=>{
                        res.statusCode = 200;
                        res.setHeader('Content-Type','application/json')
                        res.json(favs);
                       },(err)=>next(err))
                  },(err)=>next(err))
               }
             },(err)=>next(err))
             .catch((err) => next(err));
            })
            

.delete(cors.corsWithOptions,authenticate.verifyUser,(req,res,next)=>{
    Favorite.remove({})
    .then((resp) => {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(resp);
        },(err) => next(err))
    .catch((err) => next(err));
});


favRouter.route('/:dishId')
.options(cors.corsWithOptions,(req,res)=>{
    res.sendStatus(200);})
    .get(cors.cors, authenticate.verifyUser, (req,res,next) => {
        Favorite.findOne({user: req.user._id})
        .then((favorites) => {
            if (!favorites) {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                return res.json({"exists": false, "favorites": favorites});
            }
            else {
                if (favorites.dishes.indexOf(req.params.dishId) < 0) {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    return res.json({"exists": false, "favorites": favorites});
                }
                else {
                    res.statusCode = 200;
                    res.setHeader('Content-Type', 'application/json');
                    return res.json({"exists": true, "favorites": favorites});
                }
            }
    
        }, (err) => next(err))
        .catch((err) => next(err))
    })
	.put(cors.corsWithOptions, authenticate.verifyUser, (req, res, next) => {
		res.statusCode = 403;
		res.end('PUT operation not supported on /dishes');
	})
.post(cors.corsWithOptions,authenticate.verifyUser,(req,res,next)=>{
     Favorite.findOne({ user: req.user._id })
     .then((favorite) => {
        if (!favorite) {
            //create a new document and add the dishes in the request body
            Favorite.create({ user: req.user._id})//dishes: [ req.params.dishId ] }, (err, doc) => {
              .then((favs)=>{
                  favs.dishes.push({"_id":req.params.dishId})
                  favs.save()
                  .then((favs) => {
                      Favorite.findById(favs._id)
                      .populate('user')
                      .populate('dishes')
                  .then((favs)=>{
                  res.statusCode = 200;
                  res.setHeader('Content-Type','application/json')
                  res.json(favs); 
                  },(err)=>next(err))
              },(err)=>next(err))
            },(err)=>next(err))
            .catch((err) => {
                return next(err);
            });
    
         } else if (favorite.dishes.indexOf(req.params.dishId) > -1) {
            //dish already exists in favorites, return the dish with a message
            Dishes.findById(req.params.dishId)
                .then((dish) => {
                        res.statusCode = 200;
                        res.setHeader('Content-Type', 'application/json');
                        res.json(`${dish.name} is already a favorite!`);
                    },
                    (err) => next(err)
                )
                .catch((err) => {
                    return next(err);
                });
        } else if (favorite.dishes.indexOf(req.params.dishId) < 0) {
            //if dish is not a favorite, ad the user id, push the
            // dishId, and report by populating these fields
            req.body.user = req.user._id;
            favorite.dishes.push({ _id: req.params.dishId });
            favorite.save()
                  .then((favs) => {
                      Favorite.findById(favs._id)
                      .populate('user')
                      .populate('dishes')
                  .then((favs)=>{
                  res.statusCode = 200;
                  res.setHeader('Content-Type','application/json')
                  res.json(favs); 
                  },(err)=>next(err))
              },(err)=>next(err))
              .catch((err) => {
                return next(err)});
            }
        })
        .catch((err) => {
            return next(err)});
        })


.delete(cors.corsWithOptions,authenticate.verifyUser, (req,res,next)=>{
    Favorite.findOne({ user: req.user._id })
    .then((favorite) => {
        if (!favorite) {
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            return res.json(`You have no favorites to delete!`);
        } else if (favorite.dishes.indexOf(req.params.dishId) < 0) {
            //if dish is not a favorite
            Dishes.findById(req.params.dishId)
                .then((dish) => {
                        res.statusCode = 403;
                        res.setHeader('Content-Type', 'application/json');
                        return res.json(`${dish.name} it is not a favorite.`);
                    },(err) => next(err))
                .catch((err) => {
                    return next(err);
                });
        } else if (favorite.dishes.indexOf(req.params.dishId) > -1) {
            //dish exists in favorites, delete it from favorites {pull}
            favorite.dishes.pull(req.params.dishId);
            favorite.save()
            .then((favs) => {
                Favorite.findById(favs._id)
                .populate('user')
                .populate('dishes')
            .then((favs)=>{
            res.statusCode = 200;
            res.setHeader('Content-Type','application/json')
            res.json(favs); 
            },(err)=>next(err))
        },(err)=>next(err))
        .catch((err) => {
            return next(err);
        });
      }
      
        })
        .catch((err) => {
            return next(err);
        });
    
})


module.exports = favRouter;